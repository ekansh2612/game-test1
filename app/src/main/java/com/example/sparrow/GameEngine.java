package com.example.sparrow;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {
    private final String TAG = "SPARROW";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;

    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;

    // VISIBLE GAME PLAY AREA
    // These variables are set in the constructor
    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_RIGHT;
    int VISIBLE_BOTTOM;

    // SPRITES
    Square bullet;
    int SQUARE_WIDTH = 100;

    Square enemy;

    Sprite player;
    Sprite sparrow;
    Sprite cat;
    Sprite cage;


    ArrayList<Square> bullets = new ArrayList<Square>();

    // GAME STATS
    int score = 0;
    int bulletSpeed;

    public GameEngine(Context context, int screenW, int screenH) {
        super(context);

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        // setup visible game play area variables
        this.VISIBLE_LEFT = 20;
        this.VISIBLE_TOP = 10;
        this.VISIBLE_RIGHT = this.screenWidth - 20;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.8);


        // initalize sprites
        this.player = new Sprite(this.getContext(), this.VISIBLE_LEFT, 700, R.drawable.player64);
        this.sparrow = new Sprite(this.getContext(), 500, 200, R.drawable.bird64);
        this.cat = new Sprite(this.getContext(),1600,700,R.drawable.cat64);
        this.bullet = new Square(this.getContext(),this.player.getxPosition(),this.player.getyPosition(),10);
        // bullets and player will initiate at same location


        // drawing cage

                this.cage = new Sprite(this.getContext(),(this.VISIBLE_RIGHT -300),VISIBLE_TOP);






    }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            updateGame();    // updating positions of stuff
            redrawSprites(); // drawing the stuff
            controlFPS();
        }
    }

    // Game Loop methods
    public void updateGame() {

        // --------------------------------------
        // Moving cage left and right- R2
        //-----------------------------------------

        int CAGE_SPEED = 10;

        boolean movingRight = true;



        if (movingRight == true) {
            this.cage.xn = this.cage.xn + CAGE_SPEED;

        }

        else {
            this.cage.xn = this.cage.xn - CAGE_SPEED;
        }

        if (cage.xn > VISIBLE_RIGHT) {
            movingRight = false;
        }

        if (cage.xn < 0) {
            movingRight = false;
        }


        // --------------------------------------
        // Moving cat left and right- R3
        //-----------------------------------------

        int CAT_SPEED = 15;

        movingRight = true;



        if (movingRight == true) {
            this.cat.xn = this.cat.xn + CAT_SPEED;

        }

        else {
            this.cat.xn = this.cat.xn - CAT_SPEED;
        }

        if (cat.xn > VISIBLE_RIGHT / 3) {
            movingRight = false;
        }

        if (cat.xn < 0) {
            movingRight = false;
        }


        if(cage.hitbox.intersect(this.cat.hitbox)) {


            CAGE_SPEED = 0;
            CAT_SPEED = 0;
            score = score + 1;

            CharSequence text = " YOU WIN! ";
            int duration = Toast.LENGTH_SHORT;

            Context context = getContext();
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            }


        //--------------------------------------
        // Moving bird at random positions - R4
        //-------------------------------------

        int randomX = new Random().nextInt(screenWidth / 2);
        int randomY = new Random().nextInt(VISIBLE_TOP - VISIBLE_BOTTOM);   // Visible height

        this.sparrow.xn = randomX;
        this.sparrow.yn = randomY;



    }


    public void outputVisibleArea() {
        Log.d(TAG, "DEBUG: The visible area of the screen is:");
        Log.d(TAG, "DEBUG: Maximum w,h = " + this.screenWidth +  "," + this.screenHeight);
        Log.d(TAG, "DEBUG: Visible w,h =" + VISIBLE_RIGHT + "," + VISIBLE_BOTTOM);
        Log.d(TAG, "-------------------------------------");
    }



    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------

            // set the game's background color
            canvas.drawColor(Color.argb(255,255,255,255));

            // setup stroke style and width
            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setStrokeWidth(8);

            // --------------------------------------------------------
            // draw boundaries of the visible space of app
            // --------------------------------------------------------
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setColor(Color.argb(255, 0, 128, 0));

            canvas.drawRect(VISIBLE_LEFT, VISIBLE_TOP, VISIBLE_RIGHT, VISIBLE_BOTTOM, paintbrush);
            this.outputVisibleArea();

            // --------------------------------------------------------
            // draw player and sparrow
            // --------------------------------------------------------

            // 1. player
            canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

            // 2. sparrow
            canvas.drawBitmap(this.sparrow.getImage(), this.sparrow.getxPosition(), this.sparrow.getyPosition(), paintbrush);

            //3. Cat
            canvas.drawBitmap(this.cat.getImage(),this.cat.getxPosition(),this.cat.getyPosition(), paintbrush);

            // 4. Cage
            int cageLeft = this.VISIBLE_RIGHT - 300;
            int cageTop = this.VISIBLE_TOP;
            int cageRight = this.VISIBLE_RIGHT;
            int cageBottom = this.VISIBLE_TOP - 300;
            canvas.drawRect(cageLeft,cageTop,cageRight,cageBottom,paintbrush);

            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            Rect r = player.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(r, paintbrush);

            // draw hitbox for cat
            Rect c = cat.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(c,paintbrush);

            // draw hitbox on cage
            Rect cg = cage.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(cg,paintbrush);




            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            paintbrush.setTextSize(60);
            paintbrush.setStrokeWidth(5);
            String screenInfo = "Screen size: (" + this.screenWidth + "," + this.screenHeight + ")";
            canvas.drawText(screenInfo, 10, 100, paintbrush);

            // --------------------------------
            holder.unlockCanvasAndPost(canvas);


            // --------------------------------------
            // ---- Creating bullets -----------------
            //---------------------------------------

            bullets.add(bullet);
            bullets.add(1,bullet);
            bullets.add(2,bullet);
            bullets.add(3,bullet);
            bullets.add(4,bullet);



            // --------------------------------------
            // ---- Collision Detection -----------------
            //---------------------------------------

            if(bullet.hitbox.intersect(this.cage.hitbox)) {
                this.cage.yn = this.VISIBLE_BOTTOM - 300;
            }



        }

    }

    public void controlFPS() {
        try {
            gameThread.sleep(17);
        }
        catch (InterruptedException e) {

        }
    }


    // Deal with user input
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:

                break;
            case MotionEvent.ACTION_DOWN:
                if (event.getX() < this.screenWidth) {
                    // getting y coordinate of user touch
                   int PointX = (int) event.getX();
                   int PointY = (int) event.getY();


                   bullet.xn = bullet.xn + PointX;
                    bullet.yn = bullet.yn + PointY;


                }
                break;
       }
        return true;
    }

    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}

